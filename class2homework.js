//1
//CREATE AN OBJECT REPRESENTATION OF YOURSELF

let me = {
	'firstname':'Jacob',
  'lastname':'Marshall',
  'favorite food':'chicken',
  'Mom':{'firstname':'Marianne', 'lastname':'Marshall','favorite food':'pie'},
  'Dad':{'firstname':'Hal', 'lastname':'Marshall','favorite food':'noodles'}
}
console.log(me.Dad.firstname)
console.log(me.Mom['favorite food'])

//CREATE AN ARRAY TO REPRESENT THIS TIC-TAC-TOE BOARD

let tictactoe = [['-','o','-'],['-','x','o'],['x','-','x']]
tictactoe[0][2] = 'o'
for (let i=0;i<3;i++) {
	console.log(tictactoe[i])
}

//2 Assingment Date

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

let assignmentDate = '12/30/2019';
let assignmentDateObj = new Date(assignmentDate);
let assignmentDueDate = assignmentDateObj;
assignmentDueDate.setDate(assignmentDateObj.getDate() + 7);

//format: '<time datetime="2018-01-14">January 14, 2018</time>'
let fullyear = assignmentDueDate.getFullYear();
let paddedMonth = String( assignmentDueDate.getMonth() + 1).padStart(2, '0');
let paddedDate = String( assignmentDueDate.getDate()).padStart(2, '0');
let monthName = monthNames[ assignmentDueDate.getMonth() ];
let unpaddedDate = assignmentDueDate.getDate();
let assignmentDueDateHtml = '<time datetime=\"'.concat(fullyear,'-',paddedMonth,'-',paddedDate,'\">',monthName,' ',unpaddedDate,', ',fullyear,'</time>');
console.log(assignmentDueDateHtml);